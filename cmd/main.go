package main

import (
	"fmt"
	"log"
	"net"

	pb "gitlab.com/aeswibon/khelo-auth/generated/pkg/proto/services"
	"gitlab.com/aeswibon/khelo-auth/pkg/config"
	"gitlab.com/aeswibon/khelo-auth/pkg/db"
	"gitlab.com/aeswibon/khelo-auth/pkg/services"
	"gitlab.com/aeswibon/khelo-auth/pkg/utils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	c, err := config.LoadConfig()

	if err != nil {
		log.Fatalln("Failed at config", err)
	}

	h := db.Init(c.DBURL)

	jwt := utils.JwtWrapper{
		SecretKey:       c.JWTSecretKey,
		Issuer:          "go-grpc-auth-svc",
		ExpirationHours: 24 * 365,
	}

	lis, err := net.Listen("tcp", c.Port)

	if err != nil {
		log.Fatalln("Failed to listing:", err)
	}

	fmt.Println("Auth Svc on", c.Port)

	s := services.Server{
		H:   h,
		Jwt: jwt,
	}

	grpcServer := grpc.NewServer()
	pb.RegisterAuthServiceServer(grpcServer, &s)
	reflection.Register(grpcServer)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalln("Failed to serve:", err)
	}
}
