package db

import (
	"log"

	"gitlab.com/aeswibon/khelo-auth/pkg/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// Handler struct holds the db connection
type Handler struct {
	DB *gorm.DB
}

// Init creates a new db connection
func Init(url string) Handler {
	db, err := gorm.Open(postgres.Open(url), &gorm.Config{})
	if err != nil {
		log.Fatalf("Failed to connect to db: %v", err)
	}

	// Migrate the schema
	db.AutoMigrate(&models.User{})
	return Handler{db}
}
