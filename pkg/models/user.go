package models

// User struct defines the user model
type User struct {
	ID         int64  `json:"id" gorm:"primaryKey"`
	ExternalID string `json:"external_id"`
	Username   string `json:"username" gorm:"unique"`
	FirstName  string `json:"first_name"`
	LastName   string `json:"last_name"`
	Email      string `json:"email"`
	Password   string `json:"password"`
	Phone      string `json:"phone"`
	Gender     string `json:"gender"`
	Age        int64  `json:"age"`
	Role       string `json:"role"`
	Deleted    bool   `json:"deleted"`
	CreatedAt  int64  `json:"created_at"`
	UpdatedAt  int64  `json:"updated_at"`
}
