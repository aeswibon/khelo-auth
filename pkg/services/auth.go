package services

import (
	"context"
	"net/http"

	"gitlab.com/aeswibon/khelo-auth/generated/pkg/proto/messages"
	"gitlab.com/aeswibon/khelo-auth/generated/pkg/proto/services"
	"gitlab.com/aeswibon/khelo-auth/pkg/db"
	"gitlab.com/aeswibon/khelo-auth/pkg/models"
	"gitlab.com/aeswibon/khelo-auth/pkg/utils"
)

// Server struct to implement the gRPC server
type Server struct {
	services.UnimplementedAuthServiceServer
	H   db.Handler
	Jwt utils.JwtWrapper
}

// Register function to register a new user
func (s *Server) Register(ctx context.Context, req *messages.RegisterRequest) (*messages.RegisterResponse, error) {
	var user models.User

	if result := s.H.DB.Where(&models.User{Username: req.Username}).First(&user); result.Error == nil {
		return &messages.RegisterResponse{
			Status: http.StatusConflict,
			Error:  "Username already exists",
		}, nil
	}

	user.Email = req.Email
	user.Password = utils.HashPassword(req.Password)

	s.H.DB.Create(&user)

	return &messages.RegisterResponse{
		Status: http.StatusCreated,
	}, nil
}

// Login function to login a user
func (s *Server) Login(ctx context.Context, req *messages.LoginRequest) (*messages.LoginResponse, error) {
	var user models.User

	if result := s.H.DB.Where(&models.User{Email: req.Username}).First(&user); result.Error != nil {
		return &messages.LoginResponse{
			Status: http.StatusNotFound,
			Error:  "User not found",
		}, nil
	}

	match := utils.CheckPasswordHash(req.Password, user.Password)

	if !match {
		return &messages.LoginResponse{
			Status: http.StatusNotFound,
			Error:  "User not found",
		}, nil
	}

	token, _ := s.Jwt.GenerateToken(user)

	return &messages.LoginResponse{
		Status: http.StatusOK,
		Token:  token,
	}, nil
}

// Validate function to validate a user
func (s *Server) Validate(ctx context.Context, req *messages.ValidateRequest) (*messages.ValidateResponse, error) {
	claims, err := s.Jwt.ValidateToken(req.Token)

	if err != nil {
		return &messages.ValidateResponse{
			Status: http.StatusBadRequest,
			Error:  err.Error(),
		}, nil
	}

	var user models.User

	if result := s.H.DB.Where(&models.User{Email: claims.Username}).First(&user); result.Error != nil {
		return &messages.ValidateResponse{
			Status: http.StatusNotFound,
			Error:  "User not found",
		}, nil
	}

	return &messages.ValidateResponse{
		Status: http.StatusOK,
		UserID: user.ID,
	}, nil
}
