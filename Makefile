.PHONY: generate

generate: generate-messages generate-services

generate-messages:
	@echo "Compiling proto code for messages..."
	@protoc --proto_path=./ --go_out=generated --go_opt=paths=source_relative --go-grpc_out=generated --go-grpc_opt=paths=source_relative pkg/proto/messages/*.proto

generate-services:
	@echo "Compiling proto code for services..."
	@protoc --proto_path=./ --go_out=generated --go_opt=paths=source_relative --go-grpc_out=generated --go-grpc_opt=paths=source_relative pkg/proto/services/*.proto
